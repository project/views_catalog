<?php

/**
 * A views display plugin for catalogues.
 *
 * This embeds the catalog children view above the gallery view that uses it.
 * We don't use an attachment because the two views are of different types
 * (nodes and terms) and killing off *all* the node view options in the
 * attachment is prohibitive (if even possible at all).
 */

/**
 * The plugin that handles a full page.
 *
 * @ingroup views_display_plugins
 */
class views_catalog_plugin_display_catalog extends views_plugin_display_page {
  function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options);    
  }
  
  function pre_execute() {
    parent::pre_execute();    

    // hack!!!!!
    // @see render_header
    $this->set_option('header_empty', TRUE);
  }
  
  /**
   * We completely override the Page handler execute() because we 
   * have to build the embedded view after this view is built,
   * but before it is rendered.
   * 
   * The display page handler returns a normal view, but it also does
   * a drupal_set_title for the page, and does a views_set_page_view
   * on the view.
   */
  function execute() {
    // Let the world know that this is the page view we're using.
    views_set_page_view($this);
    
    // Prior to this being called, the $view should already be set to this
    // display, and arguments should be set on the view.
    $this->view->build();
    if (!empty($this->view->build_info['fail'])) {
      return drupal_not_found();
    }
    
    // Our stuff here
    // Build and execute the children view.
    // We have the argument handlers in the current view, but we don't
    // have a page yet.
    $this->execute_children_view();    
    // Back to stuff copied from the parent handler.
    
    $this->view->get_breadcrumb(TRUE);

    // And the title, which is much easier.
    drupal_set_title(filter_xss_admin($this->view->get_title()));
    
    // And now render the view.
    return $this->view->render();
  }

  /**
   * Options summary.
   */
  function options_summary(&$categories, &$options) {
    // It is very important to call the parent function here:
    parent::options_summary($categories, $options);

    // Since we're childing off the 'page' type, we'll still *call* our
    // category 'page' but let's override it so its settings say something else.
    $categories['page'] = array(
      'title' => t('Catalog page settings'),
    );
    
    $child_view = $this->get_option('child_view');
    if ($child_view == '') {
      $child_view = 'None';
    }
    
    // Define some extra settings groups.
    $options['child_view'] = array(
      'category' => 'page',
      'title' => t('Child view'),
      'value' => $child_view,
    );
    $options['inheritable_argument'] = array(
      'category' => 'page',
      'title' => t('Child view argument'),
      'value' => 'hello!',
    );
  }
  
  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['child_view'] = array('default' => '');
    $options['child_view_first_page'] = array('default' => FALSE);
    
    return $options;
  }  
  
  /**
   * Display the options form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'child_view':
        $form['#title'] .= t('Child view');
        $form['#help_topic'] = 'todo';
        
        $views = views_get_all_views();
        $options = array_combine(array_keys($views),  array_keys($views));
        
        $form['child_view'] = array(
          '#type' => 'select',
          '#options' => $options,
          '#description' => t('The view that will be embedded to show child categories, for example a term view.'),      
          '#default_value' => $this->get_option('child_view'),
        );
        $form['child_view_first_page'] = array(
          '#type' => 'checkbox',
          '#title' => t('Show only on first page of the main view.'),          
          '#default_value' => $this->get_option('child_view_first_page'),
        );
        break;
      case 'inheritable_argument':
        // Build list of this view's arguments to select from.
        $display_id = ($this->view->display_handler->is_defaulted('arguments')) ? 'default' : $this->current_display;
        $view_arguments = $this->view->display[$display_id]->display_options['arguments'];
        // Get the human-readable titles to display.
        $base_tables = $this->view->get_base_tables();
        $data = views_fetch_fields(array_keys($base_tables), 'argument');
        foreach($view_arguments as $key => $a) {
          $options[$key] = $data[$a['table'] . '.' . $key]['title'];
        }
        $form['inheritable_argument'] = array(
          '#type' => 'select',
          '#options' => $options,
          '#description' => t('The argument on this view whose value will be passed down to the embedded view.'),      
          '#default_value' => $this->get_option('inheritable_argument'),
        );
      
        break;
    }
  }
  
  function options_validate($form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_validate($form, $form_state);
    
    // todo: validate the child view:
    // - is a term view ( ? or allow users to do other wacky stuff?)
    // - has matching argument
    // - the inheritable argument is present (?)
  } 
  
  function options_submit($form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_submit($form, $form_state);
    
    switch ($form_state['section']) {
      case 'child_view':
        $this->set_option('child_view', $form_state['values']['child_view']);
        $this->set_option('child_view_first_page', $form_state['values']['child_view_first_page']);
        break;
      case 'inheritable_argument':
        $this->set_option('inheritable_argument', $form_state['values']['inheritable_argument']);
        break;
    }
  }   

  /**
   * Embed the gallery term view above the normal rendering of this view.
   */
  function Xrender() {
    
    // Render this view as normal.
    $output .= parent::render();

    return $output;
  }
  
  /**
   * Render the header of the view.
   *
   * Embed the children view.
   */
  function render_header() {
    if (!empty($this->view->result) || $this->get_option('header_empty')) {
      $output =  $this->render_textarea('header');
    }
    
    $output .= $this->render_children_view();
    
    return $output;
  }  

  /**
   * We hack the 'header_empty' option to make sure we come here.
   * See views's theme.inc and http://drupal.org/node/601510.
   * 
   * There are two special cases here:
   * - An empty view might have subgalleries. If that is the case we don't show
   * the empty text.
   * - Force the view title when no arguments are present.
   */
  function render_empty() {
    drupal_set_title(filter_xss_admin($this->view->get_title()));

    if (count($this->children_view->result) == 0) {
      return $this->render_textarea('empty');
    }
  }
  
  /**
   * Called after the current view has executed.
   *
   * This executes and renders the children view.
   */
  function execute_children_view() {
    // TODO: find the proper argument.
    //dsm('ecv');
    
    // Check the current gallery view has a tid argument.
    
    //// argh broken because not 'tid' any more!!!!
    $inheritable_argument = $this->get_option('inheritable_argument');
    
    if (isset($this->view->argument[$inheritable_argument])) {
      // This comes in as an array.
      $args = $this->view->argument[$inheritable_argument]->value;
      
      // No argument means we are at the top-level category.
      // We need to pass an argument of 0 to the embedded view so it
      // shows only the top level.
      if (count($args) == 0) {
        // Have to pass as string, otherwise it gets eaten by views validation.
        $args = array(0 => '0');
      }
      
      $child_view = $this->get_option('child_view');
      $this->children_view = views_get_view($child_view);

      // Tack THIS view onto the children view. We don't do anything with
      // this yet, but other handlers might need it.
      $this->children_view->embedding_view =& $this->view;
      
      $this->children_view_output = $this->children_view->preview('default', $args);
    }
  }
  
  /**
   * Render the children view.
   */
  function render_children_view() {
    // The children view has in fact already been rendered, in execute_children_view().
    // However, that function has to be called before the main view has a pager.
    // So checking the existence of a pager has to be done here.
    // But checking the children view has actual rows has to be done before this 
    // point -- @see render_empty().
    // So there's a paradox, and the way round it is to 
    // render it anway, and not always output it.
    
    // Only display sub-categories on first page
    if ($this->get_option('child_view_first_page')) {
      if ($this->view->pager['current_page'] != 0) { 
        return '';
      }        
    }
    
    //return 'child view output';
    return $this->children_view_output;
  }
}
