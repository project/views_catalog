<?php
/**
 * Allow 'hackable' URLs for the argument of the form
 * 'grandparentterm/parentterm/term
 * by only considering the last passed argument value.
 *
 * This argument must be placed in final position.
 * @todo: validation to check this.
 */
class views_catalog_handler_argument_term_node_tid_extended_url extends views_handler_argument_term_node_tid {
  /**
   * Set the input for this argument
   *
   * @return TRUE if it successfully validates; FALSE if it does not.
   */
  function set_argument($arg) {
    // Get the argument values.
    $arg_values = $this->view->args;
    
    // For each of the loaded view argument handlers, remove a value from the
    // array of arguments.
    foreach ($this->view->argument as $id => $argument) {
      if ($id != 'tid_extended') {
        array_shift($arg_values);
      }
    }   
    
    $final_arg = array_pop($arg_values);
    
    $this->argument = $final_arg;
    return $this->validate_arg($final_arg);
  }
}

