
Views Catalogue adds some extra components to Views to build catalogues of content.

A catalogue is a way of navigating content in a hierarchy. 

In Drupal, this is a page that shows nodes for a given taxonomy term, along with that term's child terms.
For example:
- an image gallery page shows the images in the current gallery and also the subgalleries
- shop products are grouped into categories and subcategories

With this module, you can make a catalogue with Views with the following system:
- a node view with:
  - the Catalog display type, set to embed a term view
  - a taxonomy term argument
- a companion term view, with:
  - a taxonomy parent argument

The term view is never shown as a standalone. It is embedded by the Catalog display type and uses the same value for its argument as the embeddding node view.

Instructions
------------
Term view needs:
- Taxonomy: Parent term argument
- Taxonomy: Vocabulary filter set to the vocabulary you want to use

Node view needs:
- a 'Catalog page' display
- Taxonomy: Term ID argument

Set the child view setting in the Catalog page options to the term view.

Use taxonomy redirect modules to make taxonomy term links in nodes go to the view.
Eg, redirect 'taxonomy/term/TID' to 'yourview/TID'.
