<?php
/**
 * @file views_catalog.views.inc
 * TODO: Enter file description here.
 */

/**
 * Implementation of hook_views_plugins().
 */
function views_catalog_views_plugins() {
  return array(
    'display' => array(
      /**
       * The catalog display: like a page, but with a subgallery
       * list embedded above it.
       * Uses a handler rather than a tpl file.
       */
      'catalog' => array(
        'title' => t('Catalog page'),
        'help' => t('Display the view as a gallery of images, with a URL and menu links.'),
        'parent' => 'page',
        'handler' => 'views_catalog_plugin_display_catalog',
        'theme' => 'views_view',
        'uses hook menu' => TRUE,
        'use ajax' => TRUE,
        'use pager' => TRUE,
        'accept attachments' => TRUE,
        'admin' => t('Page'),
        'help topic' => 'display-page', // TODO ??
        'path' => drupal_get_path('module', 'views_catalog'),
      ),
    ),
    );
}

/**
 * Implementation of hook_views_handlers().
 */
function views_catalog_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_catalog'),
    ),
    'handlers' => array(
      'views_catalog_plugin_display_catalog' => array(
        'parent' => 'views_plugin_display_page',
      ),
      'views_catalog_handler_argument_term_node_tid_extended_url' => array(
        'parent' => 'views_handler_argument_term_node_tid',
      )
    ),
  );
}

/**
 * Implementation of hook_views_data_alter().
 */
function views_catalog_views_data_alter(&$data) {
  // tid field
  $data['term_node']['tid_extended'] = array(
    'real field' => 'tid',  
    'title' => t('Term ID extended'),
    'help' => t('The taxonomy term ID, extended'),
    'argument' => array(
      'handler' => 'views_catalog_handler_argument_term_node_tid_extended_url',
      'name table' => 'term_data',
      'name field' => 'name',
      'empty field name' => t('Uncategorized'),
      'numeric' => TRUE,
      'skip base' => 'term_data',
    ),
  );
}